#! /usr/bin/env bash

if [ "$1" == "" ]; then
echo "Please specify the number of iterations."
exit
fi

cd "$(dirname "$0")"

BIN_PATH="../bin"

IFS=$'\n'


for run in $(join -j2 variants.txt sizes.txt)
do
    eval "$BIN_PATH/war_statgen  $run $1"\
    | mawk -F, -v RUN="$run" '\
        NR > 1 && $(NF) > 0 {\
            TOTAL+=$(NF)\
            }\
        END {\
            print RUN, (TOTAL / NR)\
    }' &
done

wait
unset IFS