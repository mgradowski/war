#! /usr/bin/env bash

if [ "$1" == "" ]; then
echo "Please specify the number of iterations."
exit
fi
cd "$(dirname "$0")"

BIN_PATH="../bin"

IFS=$'\n'



for run in $(join -j2 variants.txt sizes.txt)
do
    eval "$BIN_PATH/war_statgen $run $1"\
    | mawk -F, -v RUN="$run" '\
        BEGIN {\
            #variance\
            RANK_SUM=0;\
            RANK_SUM_SQ=0;\
            WIN_SUM=0;\
            WIN_SUM_SQ=0;\
            \
            #covariance\
            MRANK=0;\
            MWIN=0;\
            C=0;\
        }\
        NR > 1 && $(NF) > 0 {\
            # get columns\
            N=(NR-1);\
            RANK=$(NF-2);\
            WIN=$(NF-1);\
            \
            # variance\
            RANK_SQ=RANK*RANK;\
            WIN_SQ=WIN*WIN
            RANK_SUM+=RANK;\
            RANK_SUM_SQ+=RANK_SQ;\
            WIN_SUM+=WIN;\
            WIN_SUM_SQ+=WIN_SQ;\
            \
            #covariance\
            DRANK=RANK-MRANK;\
            MRANK+=DRANK/N;\
            MWIN+=(WIN-MWIN)/N;\
            C+=DRANK*(WIN-MWIN);\
        }\
        END {\
            N=(NR-1);\
            COVARIANCE=C/N;\
            RANK_MEAN=RANK_SUM/N;\
            RANK_VARIANCE=(RANK_SUM_SQ/N)-(RANK_MEAN*RANK_MEAN);\
            WIN_MEAN=WIN_SUM/N;\
            WIN_VARIANCE=(WIN_SUM_SQ/N)-(WIN_MEAN*WIN_MEAN);\
            RANK_STDDEV=sqrt(RANK_VARIANCE);\
            WIN_STDDEV=sqrt(WIN_VARIANCE);\
            CORRELATION=COVARIANCE/(RANK_STDDEV*WIN_STDDEV);\
            print RUN, CORRELATION;\
    }' &
done

wait
unset IFS