#! /usr/bin/env bash

if [ "$1" == "" ]; then
echo "Please specify the number of iterations."
exit
fi

cd "$(dirname "$0")"

BIN_PATH="../bin"

IFS=$'\n'


for run in $(join -j3 sizes.txt strategies.txt | join -j3 - strategies.txt)
do
    eval "$BIN_PATH/smartwar_statgen  $run $1"\
    | mawk -F, -v RUN="$run" '\
        NR > 1 && $(NF) > 0 {\
            TOTAL_MOVES+=$(NF);\
            TOTAL_WINS+=$(NF-1);\
            }\
        END {\
            N=NR-1;\
            print RUN, (TOTAL_MOVES / N), (TOTAL_WINS / N);\
    }' &
done

wait
unset IFS