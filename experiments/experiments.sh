#! /usr/bin/env bash

if [ "$1" == "" ]; then
echo "Please specify the number of iterations."
exit
fi
cd "$(dirname "$0")"

(time ./experiment1.sh $1 | awk '{$1=$1};1' | tr " " "," > ./experiment1.csv) 2> ./stat1.txt &
(time ./experiment2.sh $1 | awk '{$1=$1};1' | tr " " "," > ./experiment2.csv) 2> ./stat2.txt &
(time ./experiment34.sh $1 | awk '{$1=$1};1' | tr " " "," > ./experiment34.csv) 2> ./stat34.txt &

wait