DEBUG ?= 0
CC = gcc-10
SRC_DIR = ./src
INCL_PATH = $(SRC_DIR)/include
SOURCES = $(wildcard src/*.c)
OBJS = card.o deck.o hand.o gamestate.o cardstack.o game_logic.o utils.o mem_pool.o strategy.o return.o
COMMONFLAGS = -Wall -Wextra -Wpedantic -std=c11 -I$(INCL_PATH)
ifeq ($(DEBUG), 1)
    CCFLAGS = $(COMMONFLAGS) -ggdb -DDEBUG
else
	CCFLAGS = $(COMMONFLAGS) -O3 -march=native -DNDEBUG
endif

.PHONY: clean all nogui

all: nogui bin/smartwar_gui

nogui: bin/war_demo bin/war_statgen bin/smartwar_demo bin/smartwar_statgen

bin/war_demo: war_demo.o $(OBJS)
	mkdir -p bin
	$(CC) $(CCFLAGS) $^ -o $@

bin/war_statgen: war_statgen.o $(OBJS)
	mkdir -p bin
	$(CC) $(CCFLAGS) $^ -o $@

bin/smartwar_statgen: smartwar_statgen.o $(OBJS)
	mkdir -p bin
	$(CC) $(CCFLAGS) $^ -o $@

bin/smartwar_demo: smartwar_demo.o $(OBJS)
	mkdir -p bin
	$(CC) $(CCFLAGS) $^ -o $@

bin/smartwar_gui: smartwar_gui.o $(OBJS)
	mkdir -p bin
	$(CC) $(CCFLAGS) -lncurses $^ -o $@

test: test.o $(OBJS)
	mkdir -p bin
	$(CC) $(CCFLAGS) $^ -o $@


%.o: $(SRC_DIR)/%.c $(SRC_DIR)/%.d makefile
	$(CC) $(CCFLAGS) -c $(SRC_DIR)/$*.c


$(SRC_DIR)/%.d: $(SRC_DIR)/%.c
	@set -e; rm -f $@; \
	$(CC) $(CCFLAGS) -MM  $< > $@.$$$$; \
	sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' < $@.$$$$ > $@; \
	rm -f $@.$$$$


clean:
	rm -f *.o
	rm -f $(SRC_DIR)/*.d
	rm -rf ./bin
	rm -f ./experiments/*.csv

-include $(SOURCES:.c=.d)
