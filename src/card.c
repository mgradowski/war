#include "card.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

void
print_card_suit(card_suit_t suit)
{
    switch (suit) {
    case spades:
        fputs("♠", stdout);
        break;
    case hearts:
        fputs("♥", stdout);
        break;
    case diamonds:
        fputs("♦", stdout);
        break;
    case clubs:
        fputs("♣", stdout);
        break;
    }
}

void
print_card_value(card_value_t value)
{
    switch (value) {
    case two:
        fputs("2", stdout);
        break;
    case three:
        fputs("3", stdout);
        break;
    case four:
        fputs("4", stdout);
        break;
    case five:
        fputs("5", stdout);
        break;
    case six:
        fputs("6", stdout);
        break;
    case seven:
        fputs("7", stdout);
        break;
    case eight:
        fputs("8", stdout);
        break;
    case nine:
        fputs("9", stdout);
        break;
    case ten:
        fputs("10", stdout);
        break;
    case king:
        fputs("K", stdout);
        break;
    case queen:
        fputs("Q", stdout);
        break;
    case jack:
        fputs("J", stdout);
        break;
    case ace:
        fputs("A", stdout);
        break;
    }
}

void
print_card(card_t* card)
{
    print_card_suit(card->suit);
    print_card_value(card->value);
}