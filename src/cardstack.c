#include "cardstack.h"
#include "card.h"
#include "hand.h"
#include "mem_pool.h"
#include <stdio.h>
#include <stdlib.h>

cardstack_t*
get_empty_cardstack(void)
{
    cardstack_t* stack = malloc(sizeof(cardstack_t));
    stack->top = NULL;
    stack->size = 0;
    return stack;
}

void
cardstack_push_top(card_t* card, cardstack_t* stack)
{
    cardstack_node_t* node = get_or_alloc_cardstack_node();
    node->card = card;
    if (stack->size == 0) {
        node->next = NULL;
    } else {
        node->next = stack->top;
    }

    stack->top = node;
    stack->size += 1;
}

card_t*
cardstack_pop_top(cardstack_t* stack)
{
    card_t* tmp_card = stack->top->card;
    cardstack_node_t* tmp_node = stack->top;
    if (stack->size > 1) {
        stack->top = stack->top->next;
    } else {
        stack->top = NULL;
    }
    return_cardstack_node_to_pool(tmp_node);
    stack->size -= 1;
    return tmp_card;
}

card_t*
cardstack_pop_random(cardstack_t* stack)
{
    cardstack_node_t* prev = NULL;
    card_t* card;

    cardstack_node_t* node = stack->top;
    uint_fast32_t i = rand() % stack->size;

    while (i--) {
        prev = node;
        node = node->next;
    }

    if (prev) {
        prev->next = node->next;
    } else {
        stack->top = node->next;
    }

    card = node->card;
    return_cardstack_node_to_pool(node);
    stack->size -= 1;
    return card;
}

void
cardstack_invert(cardstack_t* stack)
{
    cardstack_node_t* prev = NULL;
    cardstack_node_t* curr = stack->top;
    cardstack_node_t* next = NULL;

    while (curr) {
        next = curr->next;
        curr->next = prev;
        prev = curr;
        curr = next;
    }

    stack->top = prev;
}

card_t*
cardstack_peek(cardstack_t* stack)
{
    return stack->top->card;
}

void
print_cardstack(cardstack_t* stack)
{
    cardstack_node_t* node = stack->top;
    for (uint_fast32_t i = 0; i < stack->size; i++) {
        print_card(node->card);
        fputs(" ", stdout);
        node = node->next;
    }
}

void
free_cardstack(cardstack_t* stack)
{
    while (stack->size > 0) {
        cardstack_pop_top(stack);
    }
    free(stack);
}