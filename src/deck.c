#include "deck.h"
#include "card.h"
#include <stdint.h>
#include <stdlib.h>

deck_t*
deck_top_n_values(int_fast32_t n)
{
    deck_t* deck = malloc(sizeof(deck_t));
    card_t* cards = malloc(n * NUM_SUITS * sizeof(card_t));
    int_fast32_t i, v, s;
    for (v = (int_fast32_t)ace, i = 0; v >= (NUM_VALUES - n); v--) {
        for (s = 0; s < NUM_SUITS; s++, i++) {
            cards[i].suit = (card_suit_t)(s);
            cards[i].value = (card_value_t)(v);
        }
    }

    deck->cards = cards;
    deck->size = n * NUM_SUITS;
    return deck;
}

deck_t*
deck_standard(void)
{
    return deck_top_n_values(NUM_VALUES);
}

void
print_deck(deck_t* deck)
{
    for (uint_fast32_t i = 0; i < deck->size; i++) {
        print_card(&deck->cards[i]);
    }
}

void
deck_shuffle(deck_t* deck)
{
    uint_fast32_t swap_i;
    card_t tmp;

    for (uint_fast32_t i = 0; i < deck->size; i++) {
        swap_i = rand() % deck->size;

        tmp = deck->cards[i];
        deck->cards[i] = deck->cards[swap_i];
        deck->cards[swap_i] = tmp;
    }
}

void
free_deck(deck_t* ptr)
{
    free(ptr->cards);
    free(ptr);
}
