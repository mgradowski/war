#include "game_logic.h"
#include "cardstack.h"
#include "gamestate.h"
#include <stdbool.h>

war_status_t
get_war_status(cardstack_t* stack1, cardstack_t* stack2)
{
    if (stack1->size == 0) {
        return war_undefined;
    }

    if (stack2->size == 0) {
        return war_undefined;
    }

    if (stack1->top->card->value > stack2->top->card->value) {
        return war_player1_won;
    }

    if (stack1->top->card->value < stack2->top->card->value) {
        return war_player2_won;
    }

    if (stack1->top->card->value == stack2->top->card->value) {
        return war_pending;
    }

    return war_undefined;
}

void
tick_A(gamestate_t* gs)
{
    if (gs->h1->size % 2) {
        cardstack_push_top(hand_pop_top(gs->h2), gs->stack2);
        cardstack_push_top(gs->player1_strategy(gs->h1, gs->stack2),
                              gs->stack1);
    } else {
        cardstack_push_top(hand_pop_top(gs->h1), gs->stack1);
        cardstack_push_top(gs->player2_strategy(gs->h2, gs->stack1),
                              gs->stack2);
    }
    gs->move_count += 1;
    while (get_war_status(gs->stack1, gs->stack2) == war_pending) {
        if (gs->h1->size < 2) {
            gs->status = game_player2_win;
            return;
        }
        if (gs->h2->size < 2) {
            gs->status = game_player1_win;
            return;
        }
        cardstack_push_top(hand_pop_top(gs->h1), gs->stack1);
        cardstack_push_top(hand_pop_top(gs->h1), gs->stack1);
        cardstack_push_top(hand_pop_top(gs->h2), gs->stack2);
        cardstack_push_top(hand_pop_top(gs->h2), gs->stack2);
        gs->move_count += 2;
    }
    if (get_war_status(gs->stack1, gs->stack2) == war_player1_won) {
        if (gs->h2->size == 0) {
            gs->status = game_player1_win;
            return;
        }
        gs->return_procedure(gs->stack1, gs->stack2, gs->h1);
    }
    if (get_war_status(gs->stack1, gs->stack2) == war_player2_won) {
        if (gs->h1->size == 0) {
            gs->status = game_player2_win;
            return;
        }
        gs->return_procedure(gs->stack2, gs->stack1, gs->h2);
    }
    return;
}

void
tick_B(gamestate_t* gs)
{
    bool added_cards_1to2 = false;
    bool added_cards_2to1 = false;
    cardstack_push_top(hand_pop_top(gs->h1), gs->stack1);
    cardstack_push_top(hand_pop_top(gs->h2), gs->stack2);
    gs->move_count += 1;
    while (get_war_status(gs->stack1, gs->stack2) == war_pending) {
        if (gs->h1->size == 0) {
            if (added_cards_2to1 || gs->h2->size < 4) {
                gs->status = game_player2_win;
                return;
            }
            cardstack_push_top(hand_pop_top(gs->h2), gs->stack1);
            cardstack_push_top(hand_pop_top(gs->h2), gs->stack1);
            cardstack_push_top(hand_pop_top(gs->h2), gs->stack2);
            cardstack_push_top(hand_pop_top(gs->h2), gs->stack2);
            added_cards_2to1 = true;
        } else if (gs->h2->size == 0) {
            if (added_cards_1to2 || gs->h1->size < 4) {
                gs->status = game_player1_win;
                return;
            }
            cardstack_push_top(hand_pop_top(gs->h1), gs->stack2);
            cardstack_push_top(hand_pop_top(gs->h1), gs->stack2);
            cardstack_push_top(hand_pop_top(gs->h1), gs->stack1);
            cardstack_push_top(hand_pop_top(gs->h1), gs->stack1);
            added_cards_1to2 = true;
        } else if (gs->h1->size == 1) {
            if (added_cards_2to1 || gs->h2->size < 3) {
                gs->status = game_player2_win;
                return;
            }
            cardstack_push_top(hand_pop_top(gs->h1), gs->stack1);
            cardstack_push_top(hand_pop_top(gs->h2), gs->stack2);
            cardstack_push_top(hand_pop_top(gs->h2), gs->stack1);
            cardstack_push_top(hand_pop_top(gs->h2), gs->stack2);
            added_cards_2to1 = true;

        } else if (gs->h2->size == 1) {
            if (added_cards_1to2 || gs->h1->size < 3) {
                gs->status = game_player1_win;
                return;
            }
            cardstack_push_top(hand_pop_top(gs->h1), gs->stack1);
            cardstack_push_top(hand_pop_top(gs->h2), gs->stack2);
            cardstack_push_top(hand_pop_top(gs->h1), gs->stack2);
            cardstack_push_top(hand_pop_top(gs->h1), gs->stack1);
            added_cards_1to2 = true;
        } else {
            cardstack_push_top(hand_pop_top(gs->h1), gs->stack1);
            cardstack_push_top(hand_pop_top(gs->h1), gs->stack1);
            cardstack_push_top(hand_pop_top(gs->h2), gs->stack2);
            cardstack_push_top(hand_pop_top(gs->h2), gs->stack2);
        }
        gs->move_count += 2;
    }
    if (get_war_status(gs->stack1, gs->stack2) == war_player1_won) {
        if (gs->h2->size == 0) {
            gs->status = game_player1_win;
            return;
        }
        gs->return_procedure(gs->stack1, gs->stack2, gs->h1);
    }
    if (get_war_status(gs->stack1, gs->stack2) == war_player2_won) {
        if (gs->h1->size == 0) {
            gs->status = game_player2_win;
            return;
        }
        gs->return_procedure(gs->stack2, gs->stack1, gs->h2);
    }
    return;
}