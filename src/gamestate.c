#include "gamestate.h"
#include "cardstack.h"
#include "deck.h"
#include "hand.h"
#include "mem_pool.h"
#include "return.h"
#include "strategy.h"
#include <stdlib.h>

static void
deal_cards(deck_t* deck, hand_t* h1, hand_t* h2)
{
    card_t* card_ptr;

    for (uint_fast32_t i = 0; i < deck->size; i++) {
        card_ptr = &deck->cards[i];
        if (i % 2) {
            hand_push_bottom(card_ptr, h1);
        } else {
            hand_push_bottom(card_ptr, h2);
        }
    }
}

gamestate_t*
gamestate_initialize_normal(deck_t* deck)
{
    gamestate_t* gs = malloc(sizeof(gamestate_t));
    hand_t* h1 = hand_create();
    hand_t* h2 = hand_create();
    cardstack_t* stack1 = get_empty_cardstack();
    cardstack_t* stack2 = get_empty_cardstack();

    deck_shuffle(deck);
    deal_cards(deck, h1, h2);

    gs->deck = deck;
    gs->h1 = h1;
    gs->h2 = h2;
    gs->stack1 = stack1;
    gs->stack2 = stack2;
    gs->player1_strategy = &always_first;
    gs->player2_strategy = &always_first;
    gs->return_procedure = &return_ordered;
    gs->move_count = 0;
    gs->status = game_pending;
    return gs;
}

gamestate_t*
gamestate_initialize_smart(deck_t* deck,
                           strategy_func_t* player1_strategy,
                           strategy_func_t* player2_strategy)
{
    gamestate_t* gs = gamestate_initialize_normal(deck);
    gs->player1_strategy = player1_strategy;
    gs->player2_strategy = player2_strategy;
    gs->return_procedure = &return_shuffled;
    return gs;
}

void
free_gamestate(gamestate_t* ptr)
{
    free_hand(ptr->h1);
    free_hand(ptr->h2);
    free_cardstack(ptr->stack1);
    free_cardstack(ptr->stack2);
    free(ptr);
}