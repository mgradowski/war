#include "hand.h"
#include "card.h"
#include "deck.h"
#include "mem_pool.h"
#include "utils.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

hand_t*
hand_create(void)
{
    hand_t* hand = malloc(sizeof(hand_t));
    hand->top = NULL;
    hand->bottom = NULL;
    hand->size = 0;
    return hand;
}

void
hand_push_bottom(card_t* card, hand_t* hand)
{
    hand_fifo_node_t* node = get_or_alloc_hand_node();

    node->card = card;
    node->next = NULL;

    if (hand->size == 0) {
        hand->top = node;
        hand->bottom = node;
    } else {
        hand->bottom->next = node;
        hand->bottom = node;
    }

    hand->size += 1;
}

void
hand_push_top(card_t* card, hand_t* hand)
{
    hand_fifo_node_t* node = get_or_alloc_hand_node();
    node->card = card;

    if (hand->size == 0) {
        node->next = NULL;
        hand->top = node;
        hand->bottom = node;
    } else {
        node->next = hand->top;
        hand->top = node;
    }

    hand->size += 1;
}

card_t*
hand_pop_top(hand_t* hand)
{
    hand_fifo_node_t* tmp_node = hand->top;
    card_t* tmp_card = tmp_node->card;

    if (hand->size == 1) {
        hand->top = NULL;
        hand->bottom = NULL;
    } else {
        hand->top = tmp_node->next;
    }

    return_hand_node_to_pool(tmp_node);
    hand->size -= 1;
    return tmp_card;
}

void
print_hand(hand_t* hand)
{
    hand_fifo_node_t* node;

    if (hand->top == NULL) {
        return;
    }

    node = hand->top;
    do {
        print_card(node->card);
        fputs(" ", stdout);
    } while ((node = node->next));
}

void
free_hand(hand_t* ptr)
{
    while (ptr->size) {
        hand_pop_top(ptr);
    }
    free(ptr);
}