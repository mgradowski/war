#pragma once
#define NUM_SUITS 4
#define NUM_VALUES 13

typedef enum
{
    clubs,
    diamonds,
    hearts,
    spades
} card_suit_t;

typedef enum
{
    two,
    three,
    four,
    five,
    six,
    seven,
    eight,
    nine,
    ten,
    jack,
    queen,
    king,
    ace
} card_value_t;

typedef struct
{
    card_value_t value;
    card_suit_t suit;
} card_t;

void print_card_suit(card_suit_t);

void print_card_value(card_value_t);

void
print_card(card_t*);