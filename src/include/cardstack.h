#pragma once
#include "card.h"
#include "hand.h"
#include <stdint.h>

typedef struct cardstack_node_t
{
    card_t* card;
    struct cardstack_node_t* next;
} cardstack_node_t;

typedef struct
{
    cardstack_node_t* top;
    uint_fast32_t size;
} cardstack_t;

cardstack_t*
get_empty_cardstack(void);

void
cardstack_push_top(card_t*, cardstack_t*);

card_t*
cardstack_pop_top(cardstack_t*);

card_t*
cardstack_pop_random(cardstack_t*);

card_t*
cardstack_peek(cardstack_t*);

void
cardstack_invert(cardstack_t*);

void
move_cards_to_hand(cardstack_t*, hand_t*);

void
print_cardstack(cardstack_t*);

void
free_cardstack(cardstack_t*);