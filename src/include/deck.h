#pragma once
#include "card.h"
#include <stdint.h>

#define STANDARD_DECK_SIZE (NUM_VALUES * NUM_SUITS)

typedef struct
{
    uint_fast32_t size;
    card_t* cards;
} deck_t;

deck_t* deck_top_n_values(int_fast32_t);

deck_t*
deck_standard(void);

void
deck_shuffle(deck_t*);

void
print_deck(deck_t*);

void
free_deck(deck_t*);