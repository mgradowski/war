#pragma once
#include "cardstack.h"
#include "gamestate.h"

typedef enum
{
    war_player1_won,
    war_player2_won,
    war_pending,
    war_undefined
} war_status_t;

war_status_t
get_war_status(cardstack_t*, cardstack_t*);

void
tick_A(gamestate_t*);

void
tick_B(gamestate_t*);