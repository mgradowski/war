#pragma once
#include "cardstack.h"
#include "deck.h"
#include "hand.h"
#include "return.h"
#include "strategy.h"
#include <stdint.h>

typedef enum
{
    game_player1_win,
    game_player2_win,
    game_pending
} gamestatus_t;

typedef struct
{
    deck_t* deck;
    hand_t* h1;
    hand_t* h2;
    cardstack_t* stack1;
    cardstack_t* stack2;
    strategy_func_t* player1_strategy;
    strategy_func_t* player2_strategy;
    return_func_t* return_procedure;
    uint_fast32_t move_count;
    gamestatus_t status;
} gamestate_t;

gamestate_t*
gamestate_initialize_normal(deck_t*);

gamestate_t*
gamestate_initialize_smart(deck_t*, strategy_func_t*, strategy_func_t*);

void
free_gamestate(gamestate_t*);