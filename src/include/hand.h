#pragma once
#include "card.h"
#include "deck.h"

typedef struct hand_fifo_node_t
{
    card_t* card;
    struct hand_fifo_node_t* next;
} hand_fifo_node_t;

typedef struct
{
    hand_fifo_node_t* top;
    hand_fifo_node_t* bottom;
    uint_fast32_t size;
} hand_t;

hand_t*
hand_create(void);

void
hand_push_bottom(card_t*, hand_t*);

void
hand_push_top(card_t*, hand_t*);

card_t*
hand_pop_top(hand_t*);

void
print_hand(hand_t*);

void
free_hand(hand_t*);