#pragma once
#include "cardstack.h"
#include "hand.h"

void
return_hand_node_to_pool(hand_fifo_node_t*);

hand_fifo_node_t*
get_or_alloc_hand_node(void);

void
return_cardstack_node_to_pool(cardstack_node_t*);

cardstack_node_t*
get_or_alloc_cardstack_node(void);

void
free_mem_pools(void);