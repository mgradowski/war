#pragma once
#include "cardstack.h"
#include "hand.h"

typedef void
return_func_t(cardstack_t*, cardstack_t*, hand_t*);

void
return_ordered(cardstack_t*, cardstack_t*, hand_t*);

void
return_shuffled(cardstack_t*, cardstack_t*, hand_t*);