#pragma once
#include "card.h"
#include "cardstack.h"
#include "hand.h"

typedef card_t*
strategy_func_t(hand_t*, cardstack_t*);

card_t*
always_first(hand_t*, cardstack_t*);

card_t*
random_(hand_t*, cardstack_t*);

card_t*
eager(hand_t*, cardstack_t*);

card_t*
peaceful(hand_t*, cardstack_t*);

card_t*
user_input(hand_t*, cardstack_t*);