#pragma once
#include "gamestate.h"

#define MAX_ITER_SMALL_DECK 100000

typedef void
tick_func_t(gamestate_t*);

void
print_game(gamestate_t*, tick_func_t*);

gamestatus_t
get_game_result(gamestate_t*, tick_func_t*);