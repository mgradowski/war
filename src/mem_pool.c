#include "cardstack.h"
#include "hand.h"
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

static struct
{
    hand_fifo_node_t* top_node;
    uint_fast32_t size;
} h_pool = { .top_node = NULL, .size = 0 };

static struct
{
    cardstack_node_t* top_node;
    uint_fast32_t size;
} cs_pool = { .top_node = NULL, .size = 0 };

void
return_hand_node_to_pool(hand_fifo_node_t* n)
{
    n->next = h_pool.top_node;
    h_pool.top_node = n;
    h_pool.size += 1;
}

hand_fifo_node_t*
get_or_alloc_hand_node(void)
{
    hand_fifo_node_t* n;

    if (h_pool.size == 0) {
        return malloc(sizeof(hand_fifo_node_t));
    }

    n = h_pool.top_node;
    h_pool.top_node = n->next;
    h_pool.size -= 1;
    return n;
}

void
return_cardstack_node_to_pool(cardstack_node_t* n)
{
    n->next = cs_pool.top_node;
    cs_pool.top_node = n;
    cs_pool.size += 1;
}

cardstack_node_t*
get_or_alloc_cardstack_node(void)
{
    cardstack_node_t* n;

    if (cs_pool.size == 0) {
        return malloc(sizeof(cardstack_node_t));
    }

    n = cs_pool.top_node;
    cs_pool.top_node = n->next;
    cs_pool.size -= 1;
    return n;
}

void
free_mem_pools(void)
{
    hand_fifo_node_t* h;
    cardstack_node_t* cs;

    while ((h = h_pool.top_node)) {
        h_pool.top_node = h->next;
        free(h);
    }

    while ((cs = cs_pool.top_node)) {
        cs_pool.top_node = cs->next;
        free(cs);
    }

    h_pool.size = 0;
    cs_pool.size = 0;
}