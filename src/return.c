#include "cardstack.h"
#include "hand.h"
#include <stdlib.h>

void
return_ordered(cardstack_t* stack1, cardstack_t* stack2, hand_t* hand)
{

    cardstack_invert(stack1);
    cardstack_invert(stack2);

    while (stack1->size > 0) {
        hand_push_bottom(cardstack_pop_top(stack1), hand);
    }

    while (stack2->size > 0) {
        hand_push_bottom(cardstack_pop_top(stack2), hand);
    }
}

void
return_shuffled(cardstack_t* stack1, cardstack_t* stack2, hand_t* hand)
{
    while (stack1->size > 0 && stack2->size > 0) {
        hand_push_bottom(cardstack_pop_random(rand() % 2 ? stack1 : stack2),
                         hand);
    }

    while (stack1->size > 0) {
        hand_push_bottom(cardstack_pop_random(stack1), hand);
    }

    while (stack2->size > 0) {
        hand_push_bottom(cardstack_pop_random(stack2), hand);
    }
}