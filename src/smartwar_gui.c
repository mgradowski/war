#include "card.h"
#include "deck.h"
#include "game_logic.h"
#include "gamestate.h"
#include "mem_pool.h"
#include "unistd.h"
#include <inttypes.h>
#include <locale.h>
#include <ncurses.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>

#define NOCARD NULL
#define TABLE_BACKGROUND 1
#define RED_CARD 2
#define BLACK_CARD 3
#define STACK_CARDS 4
#define ENTER 10
#define CHOICE_HIGHLIGHT 5
#define ARGV_STRATEGY2 argv[1]
#define CARD_WIDTH 5
#define CARD_HEIGHT 5
#define STATUSBAR_HEIGHT 3

static WINDOW* win_choice1;
static WINDOW* win_choice2;
static WINDOW* win_stack1;
static WINDOW* win_stack2;
static WINDOW* win_statusbar1;
static WINDOW* win_statusbar2;
static cardstack_t* stack1_ptr;
static cardstack_t* stack2_ptr;

static void
print_help()
{
    puts("Usage:\n  smartwar_gui [opponent_strategy]");
    puts("    opponent_strategy: R/E/P");
}

static void
fill_background(WINDOW* win, chtype color)
{
    wattron(win, color);
    for (int_fast32_t y = 0; y < getmaxy(win); y++) {
        for (int_fast32_t x = 0; x < getmaxx(win); x++) {
            wmove(win, y, x);
            waddstr(win, " ");
        }
    }
    wattroff(win, color);
    wrefresh(win);
}

void
show_card_suit(WINDOW* win, card_suit_t suit)
{
    switch (suit) {
    case spades:
        waddstr(win, "♠");
        break;
    case hearts:
        waddstr(win, "♥");
        break;
    case diamonds:
        waddstr(win, "♦");
        break;
    case clubs:
        waddstr(win, "♣");
        break;
    }
}

void
show_card_value(WINDOW* win, card_value_t value)
{
    switch (value) {
    case two:
        waddstr(win, "2");
        break;
    case three:
        waddstr(win, "3");
        break;
    case four:
        waddstr(win, "4");
        break;
    case five:
        waddstr(win, "5");
        break;
    case six:
        waddstr(win, "6");
        break;
    case seven:
        waddstr(win, "7");
        break;
    case eight:
        waddstr(win, "8");
        break;
    case nine:
        waddstr(win, "9");
        break;
    case ten:
        waddstr(win, "10");
        break;
    case king:
        waddstr(win, "K");
        break;
    case queen:
        waddstr(win, "Q");
        break;
    case jack:
        waddstr(win, "J");
        break;
    case ace:
        waddstr(win, "A");
        break;
    }
}

static void
show_card(WINDOW* win, card_t* card, int_fast32_t y_offset)
{
    if (card == NOCARD) {
        return;
    }
    // SET COLOR
    switch (card->suit) {
    case spades:
    case clubs:
        wattron(win, COLOR_PAIR(BLACK_CARD));
        break;

    case diamonds:
    case hearts:
        wattron(win, COLOR_PAIR(RED_CARD));
        break;
    }
    // FILL CARD
    for (uint_fast32_t y = 0; y < CARD_HEIGHT; y++) {
        for (uint_fast32_t x = 0; x < CARD_WIDTH; x++) {
            wmove(win, y + y_offset, x);
            waddstr(win, " ");
        }
    }

    // SHOW TOP LEFT VALUE
    wmove(win, y_offset, 0);
    show_card_value(win, card->value);

    // SHOW TOP RIGHT VALUE
    if (card->value == ten) {
        wmove(win, y_offset, CARD_WIDTH - 2);
        show_card_value(win, card->value);
    } else {
        wmove(win, y_offset, CARD_WIDTH - 1);
        show_card_value(win, card->value);
    }

    // SHOW BOTTOM LEFT VALUE
    wmove(win, CARD_HEIGHT - 1 + y_offset, 0);
    show_card_value(win, card->value);

    // SHOW BOTTOM RIGHT VALUE
    if (card->value == ten) {
        wmove(win, CARD_HEIGHT - 1 + y_offset, CARD_WIDTH - 2);
        show_card_value(win, card->value);
    } else {
        wmove(win, CARD_HEIGHT - 1 + y_offset, CARD_WIDTH - 1);
        show_card_value(win, card->value);
    }

    // SHOW TOP LEFT SUIT
    wmove(win, 1 + y_offset, 0);
    show_card_suit(win, card->suit);

    // SHOW TOP RIGHT SUIT
    wmove(win, 1 + y_offset, CARD_WIDTH - 1);
    show_card_suit(win, card->suit);

    // SHOW BOTTOM LEFT SUIT
    wmove(win, CARD_HEIGHT - 2 + y_offset, 0);
    show_card_suit(win, card->suit);

    // SHOW BOTTOM RIGHT SUIT
    wmove(win, CARD_HEIGHT - 2 + y_offset, CARD_WIDTH - 1);
    show_card_suit(win, card->suit);

    wrefresh(win);

    // UNSET COLOR
    switch (card->suit) {
    case spades:
    case clubs:
        wattroff(win, COLOR_PAIR(BLACK_CARD));
        break;

    case diamonds:
    case hearts:
        wattroff(win, COLOR_PAIR(RED_CARD));
        break;
    }
}

void
draw_choice_window(WINDOW* win, card_t* card, bool active)
{
    fill_background(win, COLOR_PAIR(TABLE_BACKGROUND));

    show_card(win, card, getmaxy(win) - CARD_HEIGHT - 1);
    if (active) {
        wattron(win, COLOR_PAIR(CHOICE_HIGHLIGHT));
        for (uint_fast32_t i = 0; i < CARD_WIDTH; i++) {
            wmove(win, getmaxy(win) - 1, i);
            waddstr(win, " ");
        }
        wattroff(win, COLOR_PAIR(CHOICE_HIGHLIGHT));
    }
    wrefresh(win);
}

void
draw_statusbar(WINDOW* win, hand_t* hand, cardstack_t* stack)
{
    fill_background(win, COLOR_PAIR(TABLE_BACKGROUND));
    wattron(win, COLOR_PAIR(TABLE_BACKGROUND) | A_BOLD);
    wmove(win, 1, 0);
    wprintw(win, "Cards in hand: %" PRIuFAST32, hand->size);
    wmove(win, 2, 0);
    wprintw(win, "Cards on stack: %" PRIuFAST32, stack->size);
    wattroff(win, COLOR_PAIR(TABLE_BACKGROUND) | A_BOLD);
    wrefresh(win);
}

void
draw_stack(WINDOW* win, cardstack_t* stack)
{
    if (stack->size == 0) {
        return;
    }
    fill_background(win, COLOR_PAIR(TABLE_BACKGROUND));
    show_card(
      win, stack->top->card, getmaxy(win) - (CARD_HEIGHT + stack->size));
    wattron(win, COLOR_PAIR(STACK_CARDS));
    for (uint_fast32_t y = 0; y < stack->size - 1; y++) {
        for (uint_fast32_t x = 0; x < CARD_WIDTH; x++) {
            wmove(win, getmaxy(win) - stack->size + y, x);
            waddstr(win, "▔");
        }
    }

    wattroff(win, COLOR_PAIR(STACK_CARDS));
    wrefresh(win);
}

void
draw_gamestate(gamestate_t* gs)
{
    fill_background(stdscr, COLOR_PAIR(TABLE_BACKGROUND));
    draw_statusbar(win_statusbar1, gs->h1, gs->stack1);
    draw_statusbar(win_statusbar2, gs->h2, gs->stack2);
    draw_stack(win_stack1, gs->stack1);
    draw_stack(win_stack2, gs->stack2);
}

card_t*
user_input_gui(hand_t* hand, cardstack_t* opponent_stack)
{
    (void)opponent_stack;
    draw_stack(win_stack1, stack1_ptr);
    draw_stack(win_stack2, stack2_ptr);
    if (hand->size == 1) {
        card_t* card = hand_pop_top(hand);
        draw_choice_window(win_choice1, card, true);
        getch();
        return card;
    }
    card_t* card1 = hand_pop_top(hand);
    card_t* card2 = hand_pop_top(hand);
    int choice = 0;

    bool choose_first = true;
    do {
        if (choice == KEY_LEFT || choice == KEY_RIGHT) {
            choose_first = !choose_first;
        }
        draw_choice_window(win_choice1, card1, choose_first);
        draw_choice_window(win_choice2, card2, !choose_first);
    } while ((choice = getch()) != ENTER);

    if (choose_first) {
        hand_push_top(card2, hand);
        return card1;
    } else {
        hand_push_top(card1, hand);
        return card2;
    }
}

void
tick_gui(gamestate_t* gs)
{
    usleep(250000);
    if (gs->h1->size % 2) {
        cardstack_push_top(hand_pop_top(gs->h2), gs->stack2);
        draw_statusbar(win_statusbar2, gs->h2, gs->stack2);
        cardstack_push_top(gs->player1_strategy(gs->h1, gs->stack2),
                           gs->stack1);
    } else {
        cardstack_push_top(hand_pop_top(gs->h1), gs->stack1);
        cardstack_push_top(gs->player2_strategy(gs->h2, gs->stack1),
                           gs->stack2);
    }
    draw_gamestate(gs);
    usleep(500000);
    gs->move_count += 1;
    while (get_war_status(gs->stack1, gs->stack2) == war_pending) {
        if (gs->h1->size < 2) {
            gs->status = game_player2_win;
            return;
        }
        if (gs->h2->size < 2) {
            gs->status = game_player1_win;
            return;
        }
        cardstack_push_top(hand_pop_top(gs->h1), gs->stack1);
        cardstack_push_top(hand_pop_top(gs->h1), gs->stack1);
        cardstack_push_top(hand_pop_top(gs->h2), gs->stack2);
        cardstack_push_top(hand_pop_top(gs->h2), gs->stack2);
        gs->move_count += 2;
        draw_gamestate(gs);
        usleep(500000);
    }
    if (get_war_status(gs->stack1, gs->stack2) == war_player1_won) {
        if (gs->h2->size == 0) {
            gs->status = game_player1_win;
            return;
        }
        gs->return_procedure(gs->stack1, gs->stack2, gs->h1);
    }
    if (get_war_status(gs->stack1, gs->stack2) == war_player2_won) {
        if (gs->h1->size == 0) {
            gs->status = game_player2_win;
            return;
        }
        gs->return_procedure(gs->stack2, gs->stack1, gs->h2);
    }
    return;
}

int
main(int argc, char** argv)
{
    if (argc != 2) {
        print_help();
        exit(EXIT_FAILURE);
    }
    srand(time(NULL));
    char strategy2_arg;
    strategy_func_t* strategy2;

    if (!sscanf(ARGV_STRATEGY2, " %c", &strategy2_arg)) {
        print_help();
        exit(EXIT_FAILURE);
    }

    switch (strategy2_arg) {
    case 'R':
        strategy2 = random_;
        break;
    case 'E':
        strategy2 = eager;
        break;
    case 'P':
        strategy2 = peaceful;
        break;
    default:
        print_help();
        exit(EXIT_FAILURE);
        break;
    }

    setlocale(LC_ALL, "");
    // init ncurses
    initscr();
    noecho();
    keypad(stdscr, TRUE);
    start_color();
    // init colors
    init_pair(TABLE_BACKGROUND, COLOR_WHITE, COLOR_GREEN);
    init_pair(STACK_CARDS, COLOR_BLACK, COLOR_WHITE);
    init_pair(BLACK_CARD, COLOR_BLACK, COLOR_WHITE);
    init_pair(RED_CARD, COLOR_RED, COLOR_WHITE);
    init_pair(CHOICE_HIGHLIGHT, COLOR_BLUE, COLOR_BLUE);
    // create windows
    int window_height;
    int window_width;
    getmaxyx(stdscr, window_height, window_width);

    win_choice1 = newwin(window_height - STATUSBAR_HEIGHT, CARD_WIDTH, 0, 1);
    win_choice2 =
      newwin(window_height - STATUSBAR_HEIGHT, CARD_WIDTH, 0, CARD_WIDTH + 2);
    win_stack1 = newwin(window_height - STATUSBAR_HEIGHT,
                        CARD_WIDTH,
                        0,
                        window_width / 2 - (CARD_WIDTH + 1));
    win_stack2 = newwin(
      window_height - STATUSBAR_HEIGHT, CARD_WIDTH, 0, window_width / 2 + 1);
    win_statusbar1 = newwin(
      STATUSBAR_HEIGHT, window_width / 2, window_height - STATUSBAR_HEIGHT, 0);
    win_statusbar2 = newwin(STATUSBAR_HEIGHT,
                            window_width / 2,
                            window_height - STATUSBAR_HEIGHT,
                            window_width / 2);

    deck_t* deck = deck_standard();
    gamestate_t* gs =
      gamestate_initialize_smart(deck, user_input_gui, strategy2);
    stack1_ptr = gs->stack1;
    stack2_ptr = gs->stack2;
    uint_fast32_t turn_count = 1;
    while (gs->status == game_pending) {
        draw_gamestate(gs);
        tick_gui(gs);
        turn_count++;
    }

    delwin(win_choice1);
    delwin(win_choice2);
    endwin();
    free_gamestate(gs);
    free_deck(deck);
    free_mem_pools();
    return EXIT_SUCCESS;
}