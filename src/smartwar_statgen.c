#include "card.h"
#include "deck.h"
#include "game_logic.h"
#include "gamestate.h"
#include "hand.h"
#include "mem_pool.h"
#include "return.h"
#include "strategy.h"
#include "utils.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define ARGV_VALUES_COUNT argv[1]
#define ARGV_STRATEGY1 argv[2]
#define ARGV_STRATEGY2 argv[3]
#define ARGV_ITERATIONS argv[4]

#define FIELD_SEPARATOR ","

static void
print_help()
{
    puts("Usage:\n  smartwar_demo [deck] [strategy1] [strategy2] [iterations]");
    puts("    deck: 5-13");
    puts("    strategy1/2: R/E/P");
    puts("    iterations: >0");
}

static void
print_value_counts(hand_t* hand)
{
    hand_fifo_node_t* node = hand->top;
    uint_fast32_t value_counts[NUM_VALUES] = { 0 };
    while (node) {
        value_counts[node->card->value]++;
        node = node->next;
    }
    for (uint_fast32_t i = 0; i < NUM_VALUES; i++) {
        printf("%" PRIuFAST32 FIELD_SEPARATOR, value_counts[i]);
    }
}

int
main(int argc, char** argv)
{
    srand(time(NULL));
    int_fast32_t values_count;
    char strategy1_arg;
    char strategy2_arg;
    strategy_func_t* strategy1;
    strategy_func_t* strategy2;
    uint_fast32_t iterations;

    if (argc != 5) {
        print_help();
        exit(EXIT_FAILURE);
    }

    if (!sscanf(ARGV_VALUES_COUNT, " %" SCNdFAST32, &values_count)) {
        print_help();
        exit(EXIT_FAILURE);
    }

    if (!sscanf(ARGV_STRATEGY1, " %c", &strategy1_arg)) {
        print_help();
        exit(EXIT_FAILURE);
    }

    if (!sscanf(ARGV_STRATEGY2, " %c", &strategy2_arg)) {
        print_help();
        exit(EXIT_FAILURE);
    }

    if (!sscanf(ARGV_ITERATIONS, " %" SCNuFAST32, &iterations)) {
        print_help();
        exit(EXIT_FAILURE);
    }

    if (values_count > 13 || values_count < 5) {
        print_help();
        exit(EXIT_FAILURE);
    }

    switch (strategy1_arg) {
    case 'R':
        strategy1 = random_;
        break;
    case 'E':
        strategy1 = eager;
        break;
    case 'P':
        strategy1 = peaceful;
        break;
    default:
        print_help();
        exit(EXIT_FAILURE);
        break;
    }

    switch (strategy2_arg) {
    case 'R':
        strategy2 = random_;
        break;
    case 'E':
        strategy2 = eager;
        break;
    case 'P':
        strategy2 = peaceful;
        break;
    default:
        print_help();
        exit(EXIT_FAILURE);
        break;
    }

    deck_t* deck = deck_top_n_values(values_count);
    gamestate_t* gs;

    srand(time(NULL));
    // create table header
    for (uint_fast32_t t = 0; t < NUM_VALUES; t++) {
        print_card_value((card_value_t)t);
        fputs(FIELD_SEPARATOR, stdout);
    }
    fputs("WIN" FIELD_SEPARATOR "MOVES\n", stdout);

    // print card counts and resulting games
    for (uint_fast32_t i = 0; i < iterations; i++) {
        gs = gamestate_initialize_smart(deck, strategy1, strategy2);

        print_value_counts(gs->h1);
        printf("%d" FIELD_SEPARATOR,
               get_game_result(gs, &tick_A) == game_player1_win);
        printf("%" PRIuFAST32 "\n", gs->move_count);

        free_gamestate(gs);
    }

    free_deck(deck);
    free_mem_pools();

    return EXIT_SUCCESS;
}