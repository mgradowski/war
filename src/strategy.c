#include "card.h"
#include "cardstack.h"
#include "hand.h"
#include <stdio.h>
#include <stdlib.h>

card_t*
always_first(hand_t* hand, cardstack_t* opponent_stack)
{
    (void)opponent_stack;
    return hand_pop_top(hand);
}

card_t*
random_(hand_t* hand, cardstack_t* opponent_stack)
{
    if (hand->size == 1) {
        return hand_pop_top(hand);
    }
    card_t* card1 = hand_pop_top(hand);
    card_t* card2 = hand_pop_top(hand);
    (void)opponent_stack;

    if (rand() % 2) {
        hand_push_top(card2, hand);
        return card1;
    } else {
        hand_push_top(card1, hand);
        return card2;
    }
}

card_t*
eager(hand_t* hand, cardstack_t* opponent_stack)
{
    if (hand->size == 1) {
        return hand_pop_top(hand);
    }
    card_t* card1 = hand_pop_top(hand);
    card_t* card2 = hand_pop_top(hand);
    card_t* opponent_card = cardstack_peek(opponent_stack);

    if (card1->value == opponent_card->value) {
        hand_push_top(card2, hand);
        return card1;
    } else if (card2->value == opponent_card->value) {
        hand_push_top(card1, hand);
        return card2;
    } else if (card1->value > opponent_card->value &&
               card2->value > opponent_card->value) {
        if (card1->value < card2->value) {
            hand_push_top(card2, hand);
            return card1;
        } else {
            hand_push_top(card1, hand);
            return card2;
        }
    } else if (card1->value < card2->value) {
        hand_push_top(card2, hand);
        return card1;
    } else {
        hand_push_top(card1, hand);
        return card2;
    }
}

card_t*
peaceful(hand_t* hand, cardstack_t* opponent_stack)
{
    if (hand->size == 1) {
        return hand_pop_top(hand);
    }
    card_t* card1 = hand_pop_top(hand);
    card_t* card2 = hand_pop_top(hand);
    card_t* opponent_card = cardstack_peek(opponent_stack);

    if (card1->value != opponent_card->value &&
        card2->value != opponent_card->value) {
        if (card1->value < card2->value) {
            hand_push_top(card2, hand);
            return card1;
        } else {
            hand_push_top(card1, hand);
            return card2;
        }
    } else if (card2->value != opponent_card->value) {
        hand_push_top(card1, hand);
        return card2;
    } else {
        hand_push_top(card2, hand);
        return card1;
    }
}

card_t*
user_input(hand_t* hand, cardstack_t* opponent_stack)
{
    if (hand->size == 1) {
        return hand_pop_top(hand);
    }
    card_t* card1 = hand_pop_top(hand);
    card_t* card2 = hand_pop_top(hand);
    card_t* opponent_card = cardstack_peek(opponent_stack);
    char choice = 0;

    fputs("\nOpponent card: ", stdout);
    print_card(opponent_card);
    fputs("\n1st choice: ", stdout);
    print_card(card1);
    fputs("\n2nd choice: ", stdout);
    print_card(card2);
    fputs("\n", stdout);
    while (choice != '1' && choice != '2') {
        fputs("Your choice (1/2): ", stdout);
        scanf(" %c", &choice);
    }
    if (choice == '1') {
        hand_push_top(card2, hand);
        return card1;
    } else {
        hand_push_top(card1, hand);
        return card2;
    }
}