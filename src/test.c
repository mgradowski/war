#include "card.h"
#include "cardstack.h"
#include "game_logic.h"
#include "gamestate.h"
#include "hand.h"
#include <stdio.h>
#include <stdlib.h>

card_t CQ = { .value = queen, .suit = clubs };

card_t DK = { .value = king, .suit = diamonds };

card_t CJ = { .value = jack, .suit = clubs };

card_t C7 = { .value = seven, .suit = clubs };

card_t SK = { .value = king, .suit = spades };

card_t HQ = { .value = queen, .suit = hearts };

card_t C2 = { .value = two, .suit = clubs };

card_t SQ = { .value = queen, .suit = spades };

card_t H10 = { .value = ten, .suit = hearts };

card_t S10 = { .value = ten, .suit = spades };

card_t SJ = { .value = jack, .suit = spades };

card_t H2 = { .value = two, .suit = hearts };

card_t HK = { .value = king, .suit = hearts };

void
test_variantB_1()
{
    // create structures
    hand_t* h1 = hand_create();
    hand_t* h2 = hand_create();
    cardstack_t* stack1 = get_empty_cardstack();
    cardstack_t* stack2 = get_empty_cardstack();
    gamestate_t* gs = malloc(sizeof(gamestate_t));
    gs->h1 = h1;
    gs->h2 = h2;
    gs->stack1 = stack1;
    gs->stack2 = stack2;
    gs->status = game_pending;

    // test setup conext
    hand_push_bottom(&CQ, gs->h1);
    hand_push_bottom(&DK, gs->h1);
    hand_push_bottom(&CJ, gs->h1);
    hand_push_bottom(&C7, gs->h1);
    hand_push_bottom(&SK, gs->h1);
    hand_push_bottom(&HQ, gs->h1);
    hand_push_bottom(&C2, gs->h1);

    hand_push_bottom(&SQ, gs->h2);
    hand_push_bottom(&H10, gs->h2);
    hand_push_bottom(&S10, gs->h2);

    while (gs->status == game_pending) {
        tick_B(gs);
    }
    if (gs->status == game_player1_win) {
        printf("correct p1win\n");
    }
    if (gs->status == game_player2_win) {
        printf("incorrect p2win\n");
    }

    free_gamestate(gs);
}

void
test_variantB_3()
{
    // create structures
    hand_t* h1 = hand_create();
    hand_t* h2 = hand_create();
    cardstack_t* stack1 = get_empty_cardstack();
    cardstack_t* stack2 = get_empty_cardstack();
    gamestate_t* gs = malloc(sizeof(gamestate_t));
    gs->h1 = h1;
    gs->h2 = h2;
    gs->stack1 = stack1;
    gs->stack2 = stack2;
    gs->status = game_pending;

    // test setup conext
    hand_push_bottom(&CQ, gs->h1);
    hand_push_bottom(&DK, gs->h1);
    hand_push_bottom(&CJ, gs->h1);
    hand_push_bottom(&C7, gs->h1);
    hand_push_bottom(&SK, gs->h1);

    hand_push_bottom(&SQ, gs->h2);
    hand_push_bottom(&H10, gs->h2);
    hand_push_bottom(&SJ, gs->h2);
    hand_push_bottom(&H2, gs->h2);

    while (gs->status == game_pending) {
        tick_B(gs);
    }
    if (gs->status == game_player1_win) {
        printf("correct p1win\n");
    }
    if (gs->status == game_player2_win) {
        printf("incorrect p2win\n");
    }

    free_gamestate(gs);
}

void
test_variantB_4()
{
    // create structures
    hand_t* h1 = hand_create();
    hand_t* h2 = hand_create();
    cardstack_t* stack1 = get_empty_cardstack();
    cardstack_t* stack2 = get_empty_cardstack();
    gamestate_t* gs = malloc(sizeof(gamestate_t));
    gs->h1 = h1;
    gs->h2 = h2;
    gs->stack1 = stack1;
    gs->stack2 = stack2;
    gs->status = game_pending;

    // test setup conext
    hand_push_bottom(&CQ, gs->h1);
    hand_push_bottom(&DK, gs->h1);
    hand_push_bottom(&CJ, gs->h1);
    hand_push_bottom(&C7, gs->h1);
    hand_push_bottom(&SK, gs->h1);
    hand_push_bottom(&HK, gs->h1);
    hand_push_bottom(&C2, gs->h1);

    hand_push_bottom(&SQ, gs->h2);
    hand_push_bottom(&H10, gs->h2);
    hand_push_bottom(&SJ, gs->h2);
    hand_push_bottom(&H2, gs->h2);

    while (gs->status == game_pending) {
        tick_B(gs);
    }
    if (gs->status == game_player1_win) {
        printf("correct p1win\n");
    }
    if (gs->status == game_player2_win) {
        printf("incorrect p2win\n");
    }

    free_gamestate(gs);
}

void
test_cardstack_pop_random(unsigned int seed)
{
    printf("Pop random /w seed: %u ------------\n", seed);
    srand(seed);

    cardstack_t* stack = get_empty_cardstack();

    cardstack_push_top(&CQ, stack);
    cardstack_push_top(&DK, stack);
    cardstack_push_top(&CJ, stack);

    print_cardstack(stack);
    puts("");

    print_card(cardstack_pop_random(stack));
    puts("");
    print_card(cardstack_pop_random(stack));
    puts("");
    print_card(cardstack_pop_random(stack));
    puts("");
}

int
main()
{
    test_variantB_1();
    test_variantB_3();
    test_variantB_4();
    test_cardstack_pop_random(0);
    test_cardstack_pop_random(1);
    test_cardstack_pop_random(2);
    return 0;
}