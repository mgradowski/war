#include "utils.h"
#include "gamestate.h"
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

void
print_game(gamestate_t* gs, tick_func_t* tfunc)
{
    uint_fast32_t turn = 0;

    while (gs->status == game_pending) {
        printf("Hands before turn %" PRIuFAST32 ":\n", ++turn);

        fputs("Player 1:\n", stdout);
        print_hand(gs->h1);

        fputs("\nPlayer 2:\n", stdout);
        print_hand(gs->h2);

        fputs("\n\n", stdout);

        (*tfunc)(gs);
    }

    if (gs->status == game_player1_win) {
        puts("Player 1 won!");
    } else {
        puts("Player 2 won!");
    }
}

gamestatus_t
get_game_result(gamestate_t* gs, tick_func_t* tfunc)
{
    while (gs->status == game_pending) {

        if (gs->deck->size <= 20 && gs->move_count > MAX_ITER_SMALL_DECK) {
            // bail out in case of a likely infinite game
            break;
        }

        (*tfunc)(gs);
    }

    return gs->status;
}
