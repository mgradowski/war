#include "card.h"
#include "deck.h"
#include "game_logic.h"
#include "gamestate.h"
#include "hand.h"
#include "mem_pool.h"
#include "return.h"
#include "strategy.h"
#include "utils.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define ARGV_VARIANT argv[1]
#define ARGV_VALUES_COUNT argv[2]

static void
print_help()
{
    printf("Usage:\n    war_demo [variant:AB] [deck:5-13]\n");
}

int
main(int argc, char** argv)
{
    srand(time(NULL));
    int_fast32_t values_count;
    tick_func_t* tick_func;
    char variant;

    if (argc != 3) {
        print_help();
        exit(EXIT_FAILURE);
    }

    if (!sscanf(ARGV_VARIANT, " %c", &variant)) {
        print_help();
        exit(EXIT_FAILURE);
    }

    if (!sscanf(ARGV_VALUES_COUNT, " %" SCNdFAST32, &values_count)) {
        print_help();
        exit(EXIT_FAILURE);
    }

    if (values_count > 13 || values_count < 5) {
        print_help();
        exit(EXIT_FAILURE);
    }

    deck_t* deck = deck_top_n_values(values_count);
    gamestate_t* gs = gamestate_initialize_normal(deck);

    switch (variant) {
    case 'A':
        tick_func = &tick_A;
        break;
    case 'B':
        tick_func = &tick_B;
        break;
    default:
        print_help();
        exit(EXIT_FAILURE);
        break;
    }
    print_game(gs, tick_func);

    free_gamestate(gs);
    free_deck(deck);
    free_mem_pools();

    return EXIT_SUCCESS;
}