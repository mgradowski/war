#include "card.h"
#include "deck.h"
#include "game_logic.h"
#include "gamestate.h"
#include "hand.h"
#include "mem_pool.h"
#include "utils.h"
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>
#include <time.h>

#define FIELD_SEPARATOR ","
#define ARGV_VARIANT argv[1]
#define ARGV_VALUES_COUNT argv[2]
#define ARGV_ITERATIONS argv[3]

static void
print_value_counts(hand_t* hand)
{
    hand_fifo_node_t* node = hand->top;
    uint_fast32_t value_counts[NUM_VALUES] = { 0 };
    while (node) {
        value_counts[node->card->value]++;
        node = node->next;
    }
    for (uint_fast32_t i = 0; i < NUM_VALUES; i++) {
        printf("%" PRIuFAST32 FIELD_SEPARATOR, value_counts[i]);
    }
}

static void
print_help()
{
    printf(
      "Usage:\n    war_statgen [variant:AB] [deck:5-13] [iterations:>0]\n");
}

int
main(int argc, char** argv)
{
    srand(time(NULL));
    int_fast32_t values_count;
    uint_fast32_t iterations;
    tick_func_t* tick_func;
    char variant;

    if (argc != 4) {
        print_help();
        exit(EXIT_FAILURE);
    }

    if (!sscanf(ARGV_VARIANT, " %c", &variant)) {
        print_help();
        exit(EXIT_FAILURE);
    }

    if (!sscanf(ARGV_VALUES_COUNT, " %" SCNdFAST32, &values_count)) {
        print_help();
        exit(EXIT_FAILURE);
    }

    if (!sscanf(ARGV_ITERATIONS, " %" SCNuFAST32, &iterations)) {
        print_help();
        exit(EXIT_FAILURE);
    }

    if (values_count > 13 || values_count < 5) {
        print_help();
        exit(EXIT_FAILURE);
    }

    switch (variant) {
    case 'A':
        tick_func = &tick_A;
        break;
    case 'B':
        tick_func = &tick_B;
        break;
    default:
        print_help();
        exit(EXIT_FAILURE);
        break;
    }

    deck_t* deck = deck_top_n_values(values_count);
    gamestate_t* gs;

    srand(time(NULL));
    // create table header
    for (uint_fast32_t t = 0; t < NUM_VALUES; t++) {
        print_card_value((card_value_t)t);
        fputs(FIELD_SEPARATOR, stdout);
    }
    fputs("WIN" FIELD_SEPARATOR "MOVES\n", stdout);

    // print card counts and resulting games
    for (uint_fast32_t i = 0; i < iterations; i++) {
        gs = gamestate_initialize_normal(deck);

        print_value_counts(gs->h1);
        printf("%d" FIELD_SEPARATOR,
               get_game_result(gs, tick_func) == game_player1_win);
        printf("%" PRIuFAST32 "\n",
               gs->move_count < MAX_ITER_SMALL_DECK ? gs->move_count : 0);

        free_gamestate(gs);
    }

    free(deck);
    free_mem_pools();

    return EXIT_SUCCESS;
}